package com.oleksii.controllers;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.datatype.joda.deser.DateTimeDeserializer;
import com.microsoft.bot.connector.ConnectorClient;
import com.microsoft.bot.connector.Conversations;
import com.microsoft.bot.connector.customizations.MicrosoftAppCredentials;
import com.microsoft.bot.connector.implementation.ConnectorClientImpl;
import com.microsoft.bot.schema.models.Activity;
import com.microsoft.bot.schema.models.ConversationParameters;
import com.microsoft.bot.schema.models.ConversationResourceResponse;
import com.microsoft.bot.schema.models.ResourceResponse;
import com.oleksii.creators.ActivityCreator;
import com.oleksii.creators.ConversationCreator;
import com.oleksii.senders.ResourceResponseSender;
import java.util.List;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(path = "/api")
public class BotMessagesHandler {

	@Autowired
	private MicrosoftAppCredentials credentials;

	@Autowired
	private List<ResourceResponse> responses;

	@PostMapping(path = "/messages")
	public List<ResourceResponse> messages(@RequestBody @Valid @JsonDeserialize(using = DateTimeDeserializer.class) Activity activity) {
		System.out.print("activity.recipient.id"+activity.recipient().id());
		System.out.print("activity.recipient.name"+activity.recipient().name());
		
		ConnectorClient connector = new ConnectorClientImpl(activity.serviceUrl(), credentials);		

		Activity echoActivity = ActivityCreator.createEchoActivity(activity);
		Activity checkedActivity = ActivityCreator.createSpellCheckedActivity(activity);
		Conversations conversation = ConversationCreator.createResponseConversation(connector);

		ResourceResponse echoResponse = ResourceResponseSender.send(conversation, activity, echoActivity);
		responses.add(echoResponse);

		ResourceResponse spellCheckedResponse = ResourceResponseSender.send(conversation, activity, checkedActivity);
		responses.add(spellCheckedResponse);

		return responses;
	}
	
	
	@PostMapping(path = "/createConversation")
	public ConversationResourceResponse createConversation(@RequestBody @Valid @JsonDeserialize(using = DateTimeDeserializer.class) ConversationParameters parameters) {
		ConnectorClient connector = new ConnectorClientImpl(credentials);
		
		Conversations conversation = ConversationCreator.createResponseConversation(connector);
		
		ConversationResourceResponse response = ResourceResponseSender.createConversation(conversation,parameters);
		
		return response;
		
	}
}
